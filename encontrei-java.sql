-- phpMyAdmin SQL Dump
-- version 4.2.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jun 01, 2016 at 08:48 AM
-- Server version: 5.5.38
-- PHP Version: 5.6.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `encontrei-java`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories_service`
--

CREATE TABLE `categories_service` (
`id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories_service`
--

INSERT INTO `categories_service` (`id`, `name`) VALUES
(1, 'Serviços elétricos'),
(2, 'Informática'),
(5, 'Marcenaria'),
(6, 'Decoração');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` bigint(20) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `cpf` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `is_approved` bit(1) NOT NULL,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `birthday` varchar(255) NOT NULL,
  `genre` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `roles` varchar(255) DEFAULT NULL,
  `access` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `address`, `cpf`, `description`, `email`, `is_approved`, `name`, `password`, `birthday`, `genre`, `phone`, `roles`, `access`) VALUES
(131074, 'Rua Ivapé, Jaqueline, Belo Horizonte, MG', '11454292628', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec sed odio dui. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cras mattis consectetur purus sit amet fermentum.', 'samuelmartins.sw@gmail.com', b'1', 'Samuel Martins da Silva', '827ccb0eea8a706c4c34a16891f84e7b', '1994-07-19', 'masculino', '3191232556', NULL, 'customer');

-- --------------------------------------------------------

--
-- Table structure for table `hibernate_sequences`
--

CREATE TABLE `hibernate_sequences` (
  `sequence_name` varchar(255) DEFAULT NULL,
  `sequence_next_hi_value` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hibernate_sequences`
--

INSERT INTO `hibernate_sequences` (`sequence_name`, `sequence_next_hi_value`) VALUES
('users', 5);

-- --------------------------------------------------------

--
-- Table structure for table `providers`
--

CREATE TABLE `providers` (
  `id` bigint(20) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `cpf` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `is_approved` bit(1) NOT NULL,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `rating` double NOT NULL,
  `birthday` varchar(255) NOT NULL,
  `genre` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `roles` varchar(255) DEFAULT NULL,
  `access` varchar(255) DEFAULT NULL,
  `points` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `providers`
--

INSERT INTO `providers` (`id`, `address`, `cpf`, `description`, `email`, `is_approved`, `name`, `password`, `rating`, `birthday`, `genre`, `phone`, `roles`, `access`, `points`) VALUES
(131075, 'Rua Ivapé, Jaqueline, Belo Horizonte, MG', '11454292628', 'Donec ullamcorper nulla non metus auctor fringilla. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.', 'sam.martins@outlook.com', b'1', 'Samuel Martins da Silva', '827ccb0eea8a706c4c34a16891f84e7b', 0, '1994-07-19', 'masculino', '(31) 3333-4444', NULL, 'provider', 0);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
`id` bigint(20) NOT NULL,
  `availability` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `posted_at` date DEFAULT NULL,
  `rating` double NOT NULL,
  `realized_at` date DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `type` varchar(255) NOT NULL,
  `value` double NOT NULL,
  `category_id` bigint(20) DEFAULT NULL,
  `customer_id` bigint(20) DEFAULT NULL,
  `provider_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `availability`, `description`, `name`, `posted_at`, `rating`, `realized_at`, `status`, `type`, `value`, `category_id`, `customer_id`, `provider_id`) VALUES
(1, 'teste', 'Sed posuere consectetur est at lobortis. Aenean lacinia bibendum nulla sed consectetur.', 'Fringilla Tortor Magna', NULL, 0, NULL, 'realized', 'paid', 40, 1, 131074, 131075),
(2, 'Todos os dias às 19h', 'Donec ullamcorper nulla non metus auctor fringilla. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 'Sollicitudin Risus Condimentum', NULL, 0, NULL, NULL, 'paid', 40, 5, NULL, 131075),
(3, 'Seg as 17h', 'Descrição', 'Teste', NULL, 3, NULL, 'realized', 'free', 10, 2, 131074, 131075);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `cpf` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `is_approved` bit(1) NOT NULL,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `birthday` varchar(255) NOT NULL,
  `genre` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `roles` varchar(255) DEFAULT NULL,
  `access` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user` bigint(20) NOT NULL,
  `roles` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories_service`
--
ALTER TABLE `categories_service`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `providers`
--
ALTER TABLE `providers`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
 ADD PRIMARY KEY (`id`), ADD KEY `FK_o9srbs7yo69lj2n7x0fn0r118` (`category_id`), ADD KEY `FK_5ca5e9rtw0s6bqxa92pfqmg6i` (`customer_id`), ADD KEY `FK_hlvudgvnnunhkoou6kx7muyns` (`provider_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories_service`
--
ALTER TABLE `categories_service`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `services`
--
ALTER TABLE `services`
ADD CONSTRAINT `FK_5ca5e9rtw0s6bqxa92pfqmg6i` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`),
ADD CONSTRAINT `FK_hlvudgvnnunhkoou6kx7muyns` FOREIGN KEY (`provider_id`) REFERENCES `providers` (`id`),
ADD CONSTRAINT `FK_o9srbs7yo69lj2n7x0fn0r118` FOREIGN KEY (`category_id`) REFERENCES `categories_service` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
