package com.tecweb.models;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.tecweb.util.Md5HashUtil;

@Entity
@Table(name = "users")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class User {
  @Id
  @GeneratedValue(strategy = GenerationType.TABLE)
  private Long id;

  @NotNull
  private String name;

  @NotNull
  private String cpf;

  @NotNull
  private String email;
  
  @NotNull
  private String phone;

  private String address;
  
  @NotNull
  private String genre;
  
  @NotNull
  private String birthday;

  @NotNull
  private String password;

  private String description;

  private boolean isApproved;

  @ElementCollection(fetch = FetchType.EAGER)
  @Column(name = "roles")
  private Set<String> roles;
  
  private String access;

  /**
   * @return the id
   */
  public Long getId() {
    return id;
  }

  /**
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * @return the cpf
   */
  public String getCpf() {
    return cpf;
  }

  /**
   * @return the email
   */
  public String getEmail() {
    return email;
  }

  /**
   * @return the address
   */
  public String getAddress() {
    return address;
  }

  /**
   * @return the password
   */
  public String getPassword() {
    return password;
  }

  /**
   * @return the description
   */
  public String getDescription() {
    return description;
  }

  /**
   * @return the roles
   */
  public Set<String> getRoles() {
    return roles;
  }

  /**
   * @param id the id to set
   */
  public void setId(Long id) {
    this.id = id;
  }

  /**
   * @param name the name to set
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @param cpf the cpf to set
   */
  public void setCpf(String cpf) {
    this.cpf = cpf;
  }

  /**
   * @param email the email to set
   */
  public void setEmail(String email) {
    this.email = email;
  }

  /**
   * @param address the address to set
   */
  public void setAddress(String address) {
    this.address = address;
  }

  /**
   * @param password the password to set
   */
  public void setPassword(String password) {
    this.password = Md5HashUtil.encodeHex(password);
  }

  /**
   * @param description the description to set
   */
  public void setDescription(String description) {
    this.description = description;
  }

  /**
   * @return the isApproved
   */
  public boolean isApproved() {
    return isApproved;
  }

  /**
   * @param isApproved the isApproved to set
   */
  public void setApproved(boolean isApproved) {
    this.isApproved = isApproved;
  }

  /**
   * @param roles the roles to set
   */
  public void setRoles(Set<String> roles) {
    this.roles = roles;
  }

  public String getAccess() {
		return access;
	}

	public void setAccess(String access) {
		this.access = access;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	/**
   * @param id
   * @param name
   * @param cpf
   * @param email
   * @param address
   * @param password
   * @param description
   * @param isApproved
   */
  public User(String name, String cpf, String email, String phone, String birthday, String genre, String address, String password,
      String description, String access) {
    this.name = name;
    this.phone = phone;
    this.birthday = birthday;
    this.genre = genre;
    this.cpf = cpf;
    this.email = email;
    this.address = address;
    this.setPassword(password);
    this.description = description;
    this.isApproved = false;
    this.access = access;
  }

  public User() {}

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    User other = (User) obj;
    if (id == null) {
      if (other.id != null)
        return false;
    } else if (!id.equals(other.id))
      return false;
    return true;
  }
}
