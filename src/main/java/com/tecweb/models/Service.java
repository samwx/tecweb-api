package com.tecweb.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

import com.tecweb.exceptions.OutOfRatingLimitException;

@Entity
@Table(name = "services")
@XmlRootElement
public class Service {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @NotNull
  private String name;

  /**
   * Paid or free
   */
  @NotNull
  private String type;

  @NotNull
  private double value;

  @NotNull
  private String description;

  @ManyToOne
  @JoinColumn(name = "category_id")
  private CategoryService category;

  private double rating;

  @ManyToOne
  @JoinColumn(name = "provider_id")
  private UserProvider provider;
  
  @ManyToOne
  @JoinColumn(name = "customer_id")
  private UserCustomer customer;

  @NotNull
  private String availability;
  
  @Temporal(value=TemporalType.DATE)
  private Date postedAt;
  
  @Temporal(value=TemporalType.DATE)
  private Date realizedAt;
  
  private String status;

  /**
   * @return the id
   */
  public Long getId() {
    return id;
  }

  /**
   * @param id the id to set
   */
  public void setId(Long id) {
    this.id = id;
  }

  /**
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name the name to set
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return the type
   */
  public String getType() {
    return type;
  }

  /**
   * @param type the type to set
   */
  public void setType(String type) {
    this.type = type;
  }

  /**
   * @return the value
   */
  public double getValue() {
    return value;
  }

  /**
   * @param value the value to set
   */
  public void setValue(double value) {
    this.value = value;
  }

  /**
   * @return the description
   */
  public String getDescription() {
    return description;
  }

  /**
   * @param description the description to set
   */
  public void setDescription(String description) {
    this.description = description;
  }

  /**
   * @return the rating
   */
  public double getRating() {
    return rating;
  }

  /**
   * @param rating the rating to set
   */
  public void setRating(double rating) {
    if (rating >= 0 && rating <= 5) {
      this.rating = rating;
    } else {
      throw new OutOfRatingLimitException("O rating deve ser definido entre 0 e 5");
    }
  }

  /**
   * @return the category
   */
  public CategoryService getCategory() {
    return category;
  }

  /**
   * @param category the category to set
   */
  public void setCategory(CategoryService category) {
    this.category = category;
  }

  /**
   * @return the provider
   */
  public UserProvider getProvider() {
    return provider;
  }

  /**
   * @param provider the provider to set
   */
  public void setProvider(UserProvider provider) {
    this.provider = provider;
  }

  /**
	 * @return the customer
	 */
	public UserCustomer getCustomer() {
		return customer;
	}

	/**
	 * @param customer the customer to set
	 */
	public void setCustomer(UserCustomer customer) {
		this.customer = customer;
	}

	/**
   * @return the availability
   */
  public String getAvailability() {
    return availability;
  }

  /**
   * @param availability the availability to set
   */
  public void setAvailability(String availability) {
    this.availability = availability;
  }

  /**
   * @return the postedAt
   */
  public Date getPostedAt() {
    return postedAt;
  }

  /**
   * @param postedAt the postedAt to set
   */
  public void setPostedAt(Date postedAt) {
    this.postedAt = postedAt;
  }

  /**
   * @return the realizedAt
   */
  public Date getRealizedAt() {
    return realizedAt;
  }

  /**
   * @param realizedAt the realizedAt to set
   */
  public void setRealizedAt(Date realizedAt) {
    this.realizedAt = realizedAt;
  }

  /**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
   * @param name
   * @param type
   * @param value
   * @param description
   * @param category
   * @param rating
   * @param provider
   */
  public Service(String name, String type, double value, String description,
      CategoryService category, UserProvider provider, String availability, Date postedAt) {
    this.name = name;
    this.type = type;
    this.value = value;
    this.description = description;
    this.category = category;
    this.provider = provider;
    this.availability = availability;
    this.postedAt = new Date();
    this.status = "posted";
  } 

  /* For JPA Repository */
  public Service() {}

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Service other = (Service) obj;
    if (id == null) {
      if (other.id != null)
        return false;
    } else if (!id.equals(other.id))
      return false;
    return true;
  }
}
