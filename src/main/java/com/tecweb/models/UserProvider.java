package com.tecweb.models;

import javax.persistence.Entity;
import javax.persistence.Table;
import com.tecweb.exceptions.OutOfRatingLimitException;

@Entity
@Table(name="providers")
public class UserProvider extends User {

	private double rating;
	private double points;

	public double getRating() {
		return rating;
	}

	public void setRating(double rating) {
		if ( rating >= 0 && rating <= 5 ) {
			this.rating = rating;
		}
		else {
			throw new OutOfRatingLimitException("O rating deve ser definido entre 0 e 5");
		}
	}

	public double getPoints() {
		return points;
	}

	public void setPoints(double points) {
		this.points = points;
	}

	public UserProvider(String name, String cpf, String email, String phone, String birthday, String genre, String address, String password, String description) {
    super(name, cpf, email, phone, birthday, genre, address, password, description, "provider");
		this.setRating(0);
		this.setPoints(0);
	}
	
	public UserProvider() {
		// For JPA entity
	}
}
