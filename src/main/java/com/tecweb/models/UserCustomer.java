package com.tecweb.models;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "customers")
public class UserCustomer extends User {

  public UserCustomer(String name, String cpf, String email, String phone, String birthday, String genre, String address, String password, String description) {
    super(name, cpf, email, phone, birthday, genre, address, password, description, "customer");
  }

  public UserCustomer() {}
}
