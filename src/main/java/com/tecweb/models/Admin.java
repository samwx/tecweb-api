package com.tecweb.models;

import com.tecweb.util.Md5HashUtil;

public class Admin {
  private int id;
  private String name;
  private String email;
  private String login;
  private String password;

  /**
   * @return the id
   */
  public int getId() {
    return id;
  }

  /**
   * @param id the id to set
   */
  public void setId(int id) {
    this.id = id;
  }

  /**
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name the name to set
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return the email
   */
  public String getEmail() {
    return email;
  }

  /**
   * @param email the email to set
   */
  public void setEmail(String email) {
    this.email = email;
  }

  /**
   * @return the login
   */
  public String getLogin() {
    return login;
  }

  /**
   * @param login the login to set
   */
  public void setLogin(String login) {
    this.login = login;
  }

  /**
   * @return the password
   */
  public String getPassword() {
    return password;
  }

  /**
   * @param password the password to set
   */
  public void setPassword(String password) {
    this.password = Md5HashUtil.encodeHex(password);
  }

  /**
   * @param id
   * @param name
   * @param email
   * @param login
   * @param password
   */
  private Admin(int id, String name, String email, String login, String password) {
    this.id = id;
    this.name = name;
    this.email = email;
    this.login = login;
    this.password = password;
  }

  public void allowUser(User user) {
    user.setApproved(true);
  }

  public void disallowUser(User user) {
    user.setApproved(false);
  }

}
