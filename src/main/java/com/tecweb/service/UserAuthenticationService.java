package com.tecweb.service;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tecweb.auth.Authentication;
import com.tecweb.auth.AuthenticationCredentials;
import com.tecweb.auth.AuthenticationToken;
import com.tecweb.auth.AuthenticationTokenProvider;
import com.tecweb.exceptions.InvalieUserOrPasswordException;
import com.tecweb.models.User;
import com.tecweb.repository.UserRepository;

@Service
public class UserAuthenticationService {

  private AuthenticationTokenProvider authenticationTokenProvider;

  private UserRepository userRepository;

  @Inject
  public UserAuthenticationService(AuthenticationTokenProvider authenticationTokenProvider,
      UserRepository userRepository) {
    this.authenticationTokenProvider = authenticationTokenProvider;
    this.userRepository = userRepository;
  }

  @Transactional(readOnly = true)
  public AuthenticationToken generateToken(AuthenticationCredentials credentials) {

    final User authenticatedUser = userRepository.findByEmailAndPassword(credentials.getLogin(),
        credentials.getEncodedPassword());

    if (authenticatedUser == null) {
      throw new InvalieUserOrPasswordException(credentials.getLogin());
    }
    return generateToken(authenticatedUser);
  }

  protected AuthenticationToken generateToken(User authenticatedUser) {

    final String id = Long.toHexString(authenticatedUser.getId());
    final String email = authenticatedUser.getEmail();

    return authenticationTokenProvider.generateToken(id, email);
  }

  @Transactional(readOnly = true)
  public User findUser(Authentication authentication) {

    final Long id = Long.valueOf(authentication.getId(), 16);
    final String email = authentication.getSubject();

    return userRepository.findByIdAndEmail(id, email);
  }
}
