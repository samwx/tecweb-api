package com.tecweb.repository;

import javax.inject.Inject;

import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.tecweb.models.User;

@Component
@Profile("hsqldb")
public class HsqldbRepositoryLoader implements ApplicationListener<ContextRefreshedEvent> {

  @Inject
  private UserRepository userRepository;

  @Override
  public void onApplicationEvent(ContextRefreshedEvent event) {

    final String name = "test";
    final String email = "test@test.com";
    final String description = "test user";
    final String password = "test";
    final String cpf = "00000000171";

    //userRepository.save(new User(name, cpf, email,'', '', '', null, password, description));
  }
}
