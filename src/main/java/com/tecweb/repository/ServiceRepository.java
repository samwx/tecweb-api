package com.tecweb.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.tecweb.models.Service;

@Repository
public interface ServiceRepository extends JpaRepository<Service, Long> {
	List<Service> findByStatus(String status);
	List<Service> findByStatusAndProviderId(String status, Long id);
	List<Service> findByProviderId(Long id);
	
	List<Service> findByCustomerId(Long id);
	List<Service> findByStatusAndCustomerId(String status, Long id);
	
	List<Service> findByNameLikeAndCategoryIdAndRatingGreaterThan(String name, Long category_id, double rating);
	
	@Query(value = "SELECT * FROM services s ORDER BY RAND() limit :limit", nativeQuery = true)
	List<Service> findRandomServices(@Param("limit") int limit);
	
	@Query(value = "SELECT * FROM services s WHERE s.status = 'realized' ORDER BY s.rating ASC LIMIT :limit", nativeQuery = true )
	List<Service> findAllByOrderByRatingAsc(@Param("limit") Integer limit);
	
	@Query(value = "SELECT * FROM services s WHERE s.status = 'realized' AND s.rating != 0 ORDER BY s.rating DESC LIMIT :limit", nativeQuery = true )
	List<Service> findAllByOrderByRatingDesc(@Param("limit") Integer limit);
}
