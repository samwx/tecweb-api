package com.tecweb.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.tecweb.models.UserCustomer;

@Repository
public interface UserCustomerRepository extends JpaRepository<UserCustomer, Long> {
	@Query("SELECT c FROM UserCustomer c WHERE c.isApproved = false")
	List<UserCustomer> findUnapprovedCustomers();
}
