package com.tecweb.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.tecweb.models.UserProvider;

@Repository
public interface UserProviderRepository extends JpaRepository<UserProvider, Long> {
	@Query("SELECT p FROM UserProvider p WHERE p.isApproved = false")
	List<UserProvider> findUnapprovedProviders();
}
