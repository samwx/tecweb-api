package com.tecweb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tecweb.models.CategoryService;

@Repository
public interface CategoryServiceRepository extends JpaRepository<CategoryService, Long> {
}
