package com.tecweb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tecweb.models.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

  User findByEmailAndPassword(String email, String passowrd);

  User findByIdAndEmail(Long id, String email);
}
