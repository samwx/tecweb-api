package com.tecweb.factories;

import com.tecweb.models.User;

public interface IUserFactory {
	User createUser();
}
