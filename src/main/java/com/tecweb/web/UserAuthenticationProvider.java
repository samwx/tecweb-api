package com.tecweb.web;

import com.tecweb.auth.Authentication;
import com.tecweb.auth.AuthenticationProvider;
import com.tecweb.service.UserAuthenticationService;

public class UserAuthenticationProvider implements AuthenticationProvider {

  private static final UserAuthentication ANONYMOUS_USER = new UserAuthentication(null);

  private UserAuthenticationService authenticationUserService;

  public UserAuthenticationProvider(UserAuthenticationService authenticationUserService) {
    this.authenticationUserService = authenticationUserService;
  }

  @Override
  public Authentication authenticateAnonymous() {
    return ANONYMOUS_USER;
  }

  @Override
  public Authentication authenticate(Authentication authentication) {
    return new UserAuthentication(authenticationUserService.findUser(authentication));
  }
}
