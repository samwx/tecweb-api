package com.tecweb.web;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

import com.tecweb.auth.Authentication;
import com.tecweb.models.User;

import io.jsonwebtoken.lang.Objects;

public class UserAuthentication implements Authentication {

  private Optional<User> user;

  public UserAuthentication(User user) {
    this.user = Optional.ofNullable(user);
  }

  public User getUser() {
    return user.get();
  }

  @Override
  public boolean isAuthenticated() {
    return user.isPresent();
  }

  @Override
  public String getId() {
    return Objects.getDisplayString(user.map(User::getId));
  }

  @Override
  public String getSubject() {
    return Objects.getDisplayString(user.map(User::getEmail));
  }

  @Override
  public Collection<String> getRoles() {
    return user.map(User::getRoles).orElseGet(Collections::emptySet);
  }
}
