package com.tecweb.web.cors;

import java.io.IOException;
import java.util.Optional;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;

@Provider
@PreMatching
public class CrossDomainFilter implements ContainerResponseFilter {

  @Override
  public void filter(ContainerRequestContext requestContext,
      ContainerResponseContext responseContext) throws IOException {

    final MultivaluedMap<String, Object> headers = responseContext.getHeaders();
    final Optional<String> origin = Optional.ofNullable(requestContext.getHeaderString("Origin"));

    headers.putSingle("Access-Control-Allow-Origin", origin.orElse("*"));
    headers.putSingle("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD");
    headers.putSingle("Access-Control-Allow-Headers", "Accept, Content-Type, Authorization");
    headers.putSingle("Access-Control-Allow-Credentials", "true");
    headers.putSingle("Access-Control-Max-Age", "86400");
  }
}
