package com.tecweb.web;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.web.context.WebApplicationContext;

import com.tecweb.auth.AuthenticationFilter;
import com.tecweb.auth.AuthenticationHolder;
import com.tecweb.auth.AuthenticationProvider;
import com.tecweb.auth.AuthenticationTokenProvider;
import com.tecweb.auth.AuthorizationFeature;
import com.tecweb.service.UserAuthenticationService;
import com.tecweb.web.cors.CrossDomainFilter;

@Configuration
public class ApiConfiguration {

  private static final String SECRET_KEY = "${authentication.signature.secretKey}";

  @Bean
  public UserAuthenticationProvider authenticationProvider(
      final UserAuthenticationService authenticationUserService) {
    return new UserAuthenticationProvider(authenticationUserService);
  }

  @Bean
  public AuthenticationTokenProvider authenticationTokenProvider(
      @Value(SECRET_KEY) final String secretKey) {
    return new AuthenticationTokenProvider(secretKey);
  }

  @Bean
  @Scope(scopeName = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
  public UserAuthenticationHolder authenticationHolder() {
    return new UserAuthenticationHolder();
  }

  @Bean
  public AuthenticationFilter authenticationFilter(
      final AuthenticationTokenProvider authenticationTokenProvider,
      final AuthenticationProvider authenticationProvider,
      final AuthenticationHolder authenticationHolder) {
    return new AuthenticationFilter(authenticationTokenProvider, authenticationProvider,
        authenticationHolder);
  }

  @Bean
  public CrossDomainFilter crossDomainFilter() {
    return new CrossDomainFilter();
  }

  @Bean
  public AuthorizationFeature authorizationFeature() {
    return new AuthorizationFeature();
  }
}
