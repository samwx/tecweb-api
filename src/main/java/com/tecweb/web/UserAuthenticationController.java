package com.tecweb.web;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

import org.springframework.stereotype.Controller;

import com.tecweb.MessageResponse;
import com.tecweb.auth.Authenticated;
import com.tecweb.auth.AuthenticationCredentials;
import com.tecweb.auth.AuthenticationToken;
import com.tecweb.auth.NotAuthenticated;
import com.tecweb.models.User;
import com.tecweb.service.UserAuthenticationService;

@Controller
@Path("auth")
@Authenticated
public class UserAuthenticationController {

  @Inject
  private UserAuthenticationService userAuthenticationService;

  @Inject
  private UserAuthenticationHolder userAuthenticationHolder;

  @POST
  @Path("/login")
  @NotAuthenticated
  public MessageResponse<AuthenticationToken> login(@Valid AuthenticationCredentials credentials) {

    final AuthenticationToken authenticationToken =
        userAuthenticationService.generateToken(credentials);

    return MessageResponse.to(authenticationToken).build();
  }

  @GET
  @Path("/user")
  public MessageResponse<User> user() {
    return MessageResponse.to(userAuthenticationHolder.getAuthenticatedUser()).build();
  }
}
