package com.tecweb.web;

import com.tecweb.auth.Authentication;
import com.tecweb.auth.AuthenticationHolder;
import com.tecweb.models.User;

public class UserAuthenticationHolder implements AuthenticationHolder {

  private Authentication authentication;

  @Override
  public Authentication getAuthentication() {
    return authentication;
  }

  public User getAuthenticatedUser() {
    if (getAuthentication() instanceof UserAuthentication) {
      return ((UserAuthentication) getAuthentication()).getUser();
    }
    return null;
  }

  @Override
  public void setAuthentication(Authentication authentication) {
    this.authentication = authentication;
  }
}
