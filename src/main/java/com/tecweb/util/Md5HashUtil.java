package com.tecweb.util;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Md5HashUtil {

  private static final Charset UTF_8 = Charset.forName("UTF-8");

  private static final String MD5 = "MD5";

  public static byte[] encodeBytes(String message) {
    return encodeBytes(message.getBytes(UTF_8));
  }

  public static byte[] encodeBytes(byte[] bytes) {
    try {
      return MessageDigest.getInstance(MD5).digest(bytes);
    } catch (NoSuchAlgorithmException e) {
      throw new IllegalStateException("Why MD5 not provided?", e);
    }
  }

  public static String encodeHex(String message) {
    return encodeHex(encodeBytes(message));
  }

  public static String encodeHex(byte[] bytes) {
    final StringBuilder hex = new StringBuilder(bytes.length * 2);
    for (byte b : bytes) {
      hex.append(Integer.toHexString(b >>> 4 & 0xf));
      hex.append(Integer.toHexString(b & 0xf));
    }
    return hex.toString();
  }
}
