package com.tecweb.auth;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Optional;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class AuthenticationTokenProvider {

  private final String secretKey;

  public AuthenticationTokenProvider(final String secretKey) {
    this.secretKey = secretKey;
  }

  public String getSecretKey() {
    return secretKey;
  }

  public AuthenticationToken generateToken(final String id, final String subject) {
    final JwtBuilder builder = Jwts.builder().signWith(SignatureAlgorithm.HS256, getSecretKey());
    builder.setId(id);
    builder.setSubject(subject);
    builder.setIssuedAt(new Date());
    return new AuthenticationToken(builder.compact());
  }

  public Authentication authenticateToken(final AuthenticationToken authenticationToken) {
    final Claims claims = Jwts.parser().setSigningKey(getSecretKey())
        .parseClaimsJws(authenticationToken.getToken()).getBody();
    return new ClaimAuthentication(claims);
  }

  static class ClaimAuthentication implements Authentication {

    private Optional<Claims> claims;

    public ClaimAuthentication(Claims user) {
      this.claims = Optional.ofNullable(user);
    }

    @Override
    public boolean isAuthenticated() {
      return claims.isPresent();
    }

    @Override
    public String getId() {
      return claims.map(Claims::getId).get();
    }

    @Override
    public String getSubject() {
      return claims.map(Claims::getSubject).get();
    }

    @Override
    public Collection<String> getRoles() {
      return Collections.emptyList();
    }
  }
}
