package com.tecweb.auth;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;

import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.DynamicFeature;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.FeatureContext;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Provider
public class AuthorizationFeature implements DynamicFeature {

  private static final Logger LOGGER = LoggerFactory.getLogger(AuthorizationFeature.class);

  @Override
  public void configure(ResourceInfo resourceInfo, FeatureContext context) {
    getAnnotation(resourceInfo).ifPresent(authenticated -> {
      LOGGER.debug("Authenticated REST Resource {}", resourceInfo.getResourceMethod());
      context.register(createAuthorizationFilter(authenticated));
    });
  }

  protected AuthorizationFilter createAuthorizationFilter(Authenticated authenticated) {
    return new AuthorizationFilter(authenticated);
  }

  protected Optional<Authenticated> getAnnotation(ResourceInfo resourceInfo) {
    final Method resourceMethod = resourceInfo.getResourceMethod();
    // method override class authentication
    if (resourceMethod.isAnnotationPresent(NotAuthenticated.class)) {
      return Optional.empty();
    }
    if (resourceMethod.isAnnotationPresent(Authenticated.class)) {
      return Optional.of(resourceMethod.getAnnotation(Authenticated.class));
    }
    return Optional.ofNullable(resourceInfo.getResourceClass().getAnnotation(Authenticated.class));
  }

  @Priority(Priorities.AUTHORIZATION)
  public static class AuthorizationFilter implements ContainerRequestFilter {

    private final Collection<String> roles;

    public AuthorizationFilter(Authenticated authenticated) {
      this.roles = Arrays.asList(authenticated.roles());
    }

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
      final SecurityContext securityContext = requestContext.getSecurityContext();
      // if there is no roles, at least the user should be authenticated!
      if (!isAuthenticated(securityContext) || !isAuthorized(securityContext)) {
        requestContext.abortWith(Response.status(Status.UNAUTHORIZED).build());
      }
    }

    protected boolean isAuthenticated(SecurityContext securityContext) {
      return securityContext.getUserPrincipal() != null;
    }

    protected boolean isAuthorized(SecurityContext securityContext) {
      return roles.isEmpty() || roles.stream().anyMatch(securityContext::isUserInRole);
    }
  }
}
