package com.tecweb.auth;

import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.tecweb.util.Md5HashUtil;

public class AuthenticationCredentials {

  @Size(min = 4, max = 100)
  private String login;

  @Size(min = 4, max = 20)
  private String password;

  @JsonIgnore
  private String encodedPassword;

  public AuthenticationCredentials() {}

  @JsonCreator
  public AuthenticationCredentials(@JsonProperty("login") String login,
      @JsonProperty("password") String password) {
    this.login = login;
    this.password = password;
    this.encodedPassword = encodePassword(password);
  }

  private String encodePassword(String password) {
    if (password == null) {
      return null;
    }
    return Md5HashUtil.encodeHex(password);
  }

  public String getLogin() {
    return login;
  }

  public String getPassword() {
    return password;
  }

  public String getEncodedPassword() {
    if (encodedPassword == null && password != null) {
      encodedPassword = encodePassword(password);
    }
    return encodedPassword;
  }
}
