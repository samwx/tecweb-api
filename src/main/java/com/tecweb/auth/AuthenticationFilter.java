package com.tecweb.auth;

import java.io.IOException;
import java.security.Principal;

import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;

/**
 * Validate and provide the current {@link Authentication} to the {@link AuthenticationContext} and
 * to {@link ContainerRequestContext#setSecurityContext(SecurityContext)}
 */
@Provider
@Priority(Priorities.AUTHENTICATION)
public class AuthenticationFilter implements ContainerRequestFilter {

  private static final String AUTHORIZATION_HEADER = "Authorization";
  private static final String TOKEN_PREFIX = "Bearer ";

  private AuthenticationTokenProvider authenticationTokenProvider;

  private AuthenticationProvider authenticationProvider;

  private AuthenticationHolder authenticationHolder;

  public AuthenticationFilter(AuthenticationTokenProvider authenticationTokenProvider,
      AuthenticationProvider authenticationProvider, AuthenticationHolder authenticationHolder) {
    this.authenticationTokenProvider = authenticationTokenProvider;
    this.authenticationProvider = authenticationProvider;
    this.authenticationHolder = authenticationHolder;
  }

  @Override
  public void filter(ContainerRequestContext requestContext) throws IOException {
    final Authentication authentication =
        authenticate(requestContext.getHeaderString(AUTHORIZATION_HEADER));
    // update current holder
    authenticationHolder.setAuthentication(authentication);
    // add to JAX-RS context
    requestContext.setSecurityContext(createSecurityContext(authentication));
  }

  protected Authentication authenticate(final String authenticationHeader) {
    if (authenticationHeader == null) {
      return authenticateAnonymous();
    } else if (authenticationHeader.startsWith(TOKEN_PREFIX)) {
      return authenticateToken(createToken(TOKEN_PREFIX, authenticationHeader));
    } else {
      throw new IllegalArgumentException("Invalid authentication header");
    }
  }

  protected Authentication authenticateAnonymous() {
    return authenticationProvider.authenticateAnonymous();
  }

  protected Authentication authenticateToken(final AuthenticationToken authenticationToken) {
    return authenticationProvider
        .authenticate(authenticationTokenProvider.authenticateToken(authenticationToken));
  }

  protected AuthenticationToken createToken(final String prefix, final String token) {
    return new AuthenticationToken(token.substring(prefix.length()));
  }

  private SecurityContext createSecurityContext(final Authentication authentication) {
    final Principal prinipal = createSecurityPrincipal(authentication);
    return new SecurityContext() {
      @Override
      public boolean isUserInRole(String role) {
        return authentication.hasRole(role);
      }

      @Override
      public boolean isSecure() {
        return false;
      }

      @Override
      public Principal getUserPrincipal() {
        return prinipal;
      }

      @Override
      public String getAuthenticationScheme() {
        return null;
      }
    };
  }

  private Principal createSecurityPrincipal(Authentication authentication) {
    if (authentication.isAuthenticated()) {
      return new Principal() {
        @Override
        public String getName() {
          return authentication.getSubject();
        }
      };
    }
    return null;
  }
}


