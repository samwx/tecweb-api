package com.tecweb.auth;

import java.util.Arrays;
import java.util.Collection;

public interface Authentication {

  boolean isAuthenticated();

  String getId();

  String getSubject();

  Collection<String> getRoles();

  default boolean hasRole(String role) {
    return getRoles().contains(role);
  }

  default boolean hasAnyRole(String... expectedRoles) {
    final Collection<String> expected = Arrays.asList(expectedRoles);
    return expected.isEmpty() || expected.stream().anyMatch(this::hasRole);
  }
}
