package com.tecweb.auth;

public interface AuthenticationProvider {

  public Authentication authenticateAnonymous();

  public Authentication authenticate(Authentication authentication);
}
