package com.tecweb.auth;

public interface AuthenticationHolder {

  void setAuthentication(Authentication authentication);

  Authentication getAuthentication();
}
