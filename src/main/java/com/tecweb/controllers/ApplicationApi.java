package com.tecweb.controllers;

import java.util.Collection;

import javax.inject.Inject;
import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

/**
 * Root resource (exposed at "myresource" path)
 */
@Component
@ApplicationPath("/api")
public class ApplicationApi extends ResourceConfig {

  @Inject
  public ApplicationApi(ApplicationContext applicationContext) {
    getJaxrsProviders(applicationContext).forEach(this::register);
    getJaxrsControllers(applicationContext).forEach(this::register);
  }

  private Collection<Object> getJaxrsProviders(ApplicationContext applicationContext) {
    return applicationContext.getBeansWithAnnotation(javax.ws.rs.ext.Provider.class).values();
  }

  private Collection<Object> getJaxrsControllers(ApplicationContext applicationContext) {
    return applicationContext.getBeansWithAnnotation(javax.ws.rs.Path.class).values();
  }
}
