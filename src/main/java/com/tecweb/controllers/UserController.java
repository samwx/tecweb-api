package com.tecweb.controllers;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.stereotype.Controller;

import com.tecweb.MessageResponse;
import com.tecweb.auth.Authenticated;
import com.tecweb.models.User;
import com.tecweb.repository.UserRepository;

@Controller
@Path("user")
@Authenticated
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class UserController {

  @Inject
  private UserRepository userRepository;

  public MessageResponse<List<User>> getUsers() {
    return MessageResponse.to(userRepository.findAll()).build();
  }

  @PUT
  @Path("/approve/{id}")
  public MessageResponse<User> approveUser(@PathParam("id") Long id) {
    User user = userRepository.findOne(id);
    user.setApproved(true);

    return MessageResponse.to(userRepository.save(user)).message("Usuário aprovado").build();
  }

  @GET
  @Path("/total")
  public MessageResponse<Integer> totalUsers() {
    return MessageResponse.to(userRepository.findAll().size()).build();
  }
}
