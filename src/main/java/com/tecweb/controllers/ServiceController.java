package com.tecweb.controllers;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;

import org.springframework.stereotype.Controller;

import com.tecweb.MessageResponse;
import com.tecweb.auth.Authenticated;
import com.tecweb.auth.NotAuthenticated;
import com.tecweb.exceptions.RegistryNotFoundException;
import com.tecweb.models.Service;
import com.tecweb.repository.ServiceRepository;

@Controller
@Path("services")
@Authenticated
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ServiceController {

  @Inject
  private ServiceRepository serviceRepository;

  @GET
  public MessageResponse<List<Service>> findAll() {
    return MessageResponse.to(serviceRepository.findAll()).build();
  }

  @GET
  @Path("/{id}")
  public MessageResponse<Service> get(@PathParam("id") Long id) {
    final Service service = serviceRepository.findOne(id);
    if (service == null) {
      throw new RegistryNotFoundException();
    }
    return MessageResponse.to(serviceRepository.save(service)).build();
  }

  @GET
  @Path("/create")
  public MessageResponse<Service> create() {
    return MessageResponse.to(new Service()).build();
  }

  @POST
  @Path("/create")
  public MessageResponse<Service> create(Service service) {
    return MessageResponse.to(serviceRepository.save(service)).message("Servico criado com sucesso")
        .status(Status.CREATED).build();
  }

  @PUT
  @Path("/update")
  public MessageResponse<Service> update(Service service) {
    return MessageResponse.to(serviceRepository.save(service))
        .message("Servico atualizado com sucesso").build();
  }

  @DELETE
  @Path("/{id}")
  public MessageResponse<Service> delete(@PathParam("id") Long id) {
    final Service service = serviceRepository.findOne(id);
    if (service == null) {
      throw new RegistryNotFoundException();
    }
    serviceRepository.delete(id);
    return MessageResponse.<Service>to(null).message("Service deletado com sucesso").build();
  }
  
  @GET
  @Path("/search/{name}/{category_id}/{rating}")
  public MessageResponse<List<Service>> search(
  		@PathParam("name") String name,
  		@PathParam("category_id") Long category_id,
  		@PathParam("rating") double rating){
  	return MessageResponse.to(serviceRepository.findByNameLikeAndCategoryIdAndRatingGreaterThan("%" + name + "%", category_id, rating - 1))
  			.build();
  }
  
  @GET
  @Path("/status/{status}")
  public MessageResponse<List<Service>> searchByStatus(@PathParam("status") String status){
  	return MessageResponse.to(serviceRepository.findByStatus(status)).build();
  }
  
  @GET
  @Path("/byproviderandstatus/{provider_id}/{status}")
  public MessageResponse<List<Service>> searchByStatusAndProvider(
  		@PathParam("status") String status,
  		@PathParam("provider_id") Long provider_id){
  	
  	return MessageResponse.to(serviceRepository.findByStatusAndProviderId(status, provider_id)).build();
  }
  
  @GET
  @Path("/byprovider/{provider_id}")
  public MessageResponse<List<Service>> searchByStatusAndProvider(@PathParam("provider_id") Long provider_id){
  	return MessageResponse.to(serviceRepository.findByProviderId(provider_id)).build();
  }
  
  @GET
  @Path("/bycustomerandstatus/{customer_id}/{status}")
  public MessageResponse<List<Service>> searchByStatusAndCustomer(
  		@PathParam("status") String status,
  		@PathParam("customer_id") Long customer_id){
  	return MessageResponse.to(serviceRepository.findByStatusAndCustomerId(status, customer_id)).build();
  }
  
  @GET
  @Path("/bycustomer/{customer_id}")
  public MessageResponse<List<Service>> searchByCustomer(@PathParam("customer_id") Long customer_id){
  	return MessageResponse.to(serviceRepository.findByCustomerId(customer_id)).build();
  }
  
  @GET
  @Path("/randomservices/{limit}")
  @NotAuthenticated
  public MessageResponse<List<Service>> randomServices(@PathParam("limit") int limit){
  	return MessageResponse.to(serviceRepository.findRandomServices(limit)).build();
  }
  
  @GET
  @Path("/worse/{limit}")
  public MessageResponse<List<Service>> worseServices(@PathParam("limit") Integer limit){
  	return MessageResponse.to(serviceRepository.findAllByOrderByRatingAsc(limit)).build();
  }
  
  @GET
  @Path("/best/{limit}")
  public MessageResponse<List<Service>> bestServices(@PathParam("limit") Integer limit){
  	return MessageResponse.to(serviceRepository.findAllByOrderByRatingDesc(limit)).build();
  }
  
  @GET
  @Path("/total")
  public MessageResponse<Integer> totalServices(){
  	return MessageResponse.to(serviceRepository.findAll().size()).build();
  }
}
