package com.tecweb.controllers;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;

import org.springframework.stereotype.Controller;

import com.tecweb.MessageResponse;
import com.tecweb.auth.Authenticated;
import com.tecweb.auth.NotAuthenticated;
import com.tecweb.exceptions.RegistryNotFoundException;
import com.tecweb.models.UserCustomer;
import com.tecweb.repository.UserCustomerRepository;

@Controller
@Path("users/customers")
@Authenticated
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class UserCustomerController {

  @Inject
  private UserCustomerRepository userCustomerRepository;

  @GET
  public MessageResponse<List<UserCustomer>> findAll() {
    return MessageResponse.to(userCustomerRepository.findAll()).build();
  }

  @GET
  @Path("/{id}")
  public MessageResponse<UserCustomer> get(@PathParam("id") Long id) {
    final UserCustomer userCustomer = userCustomerRepository.findOne(id);
    if (userCustomer == null) {
      throw new RegistryNotFoundException();
    }
    return MessageResponse.to(userCustomerRepository.save(userCustomer)).build();
  }

  @GET
  @Path("/create")
  public MessageResponse<UserCustomer> create() {
    return MessageResponse.to(new UserCustomer()).build();
  }

  @POST
  @NotAuthenticated
  @Path("/create")
  public MessageResponse<UserCustomer> create(UserCustomer userCustomer) {
    return MessageResponse.to(userCustomerRepository.save(userCustomer))
        .message("UserCustomer criado com sucesso").status(Status.CREATED).build();
  }

  @PUT
  @Path("/update")
  public MessageResponse<UserCustomer> update(UserCustomer userCustomer) {
    return MessageResponse.to(userCustomerRepository.save(userCustomer))
        .message("Servico atualizado com sucesso").build();
  }

  @DELETE
  @Path("/{id}")
  public MessageResponse<UserCustomer> delete(@PathParam("id") Long id) {
    final UserCustomer userCustomer = userCustomerRepository.findOne(id);
    if (userCustomer == null) {
      throw new RegistryNotFoundException();
    }
    userCustomerRepository.delete(id);
    return MessageResponse.<UserCustomer>to(null).message("UserCustomer deletado com sucesso")
        .build();
  }
  
  @GET
  @Path("/total")
  public MessageResponse<Integer> totalUsers() {
  	return MessageResponse.to(userCustomerRepository.findAll().size()).build();
  }
  
  @GET
  @Path("/unapproved")
  public MessageResponse<List<UserCustomer>> unapprovedCustomers() {
  	return MessageResponse.to(userCustomerRepository.findUnapprovedCustomers()).build();
  }
}
