package com.tecweb.controllers;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.tecweb.MessageResponse;
import com.tecweb.auth.Authenticated;
import com.tecweb.auth.NotAuthenticated;
import com.tecweb.exceptions.RegistryNotFoundException;
import com.tecweb.models.UserProvider;
import com.tecweb.repository.UserProviderRepository;

@Controller
@Path("users/providers")
@Authenticated
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class UserProviderController {

  @Autowired
  private UserProviderRepository userProviderRepository;

  @GET
  public MessageResponse<List<UserProvider>> findAll() {
    return MessageResponse.to(userProviderRepository.findAll()).build();
  }

  @GET
  @Path("/{id}")
  public MessageResponse<UserProvider> get(@PathParam("id") Long id) {
    final UserProvider userProvider = userProviderRepository.findOne(id);
    if (userProvider == null) {
      throw new RegistryNotFoundException();
    }
    return MessageResponse.to(userProviderRepository.save(userProvider)).build();
  }

  @GET
  @Path("/create")
  public MessageResponse<UserProvider> create() {
    return MessageResponse.to(new UserProvider()).build();
  }

  @POST
  @NotAuthenticated
  @Path("/create")
  public MessageResponse<UserProvider> create(UserProvider userProvider) {
    return MessageResponse.to(userProviderRepository.save(userProvider))
        .message("UserProvider criado com sucesso").status(Status.CREATED).build();
  }

  @PUT
  @Path("/update")
  public MessageResponse<UserProvider> update(UserProvider userProvider) {
    return MessageResponse.to(userProviderRepository.save(userProvider))
        .message("Servico atualizado com sucesso").build();
  }

  @DELETE
  @Path("/{id}")
  public MessageResponse<UserProvider> delete(@PathParam("id") Long id) {
    final UserProvider userProvider = userProviderRepository.findOne(id);
    if (userProvider == null) {
      throw new RegistryNotFoundException();
    }
    userProviderRepository.delete(id);
    return MessageResponse.<UserProvider>to(null).message("UserProvider deletado com sucesso")
        .build();
  }
  
  @GET
  @Path("/total")
  public MessageResponse<Integer> totalUsers() {
  	return MessageResponse.to(userProviderRepository.findAll().size()).build();
  }
  
  @GET
  @Path("/unapproved")
  public MessageResponse<List<UserProvider>> unapprovedProviders(){
  	return MessageResponse.to(userProviderRepository.findUnapprovedProviders()).build();
  }
}
