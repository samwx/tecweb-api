package com.tecweb.controllers;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

import org.springframework.stereotype.Controller;

@Controller
@Path("/")
public class ApplicationInfo {

  @GET
  public String getUsers() {
    return "ok";
  }
}
