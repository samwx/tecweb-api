package com.tecweb.controllers;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;

import org.springframework.stereotype.Controller;

import com.tecweb.MessageResponse;
import com.tecweb.auth.Authenticated;
import com.tecweb.exceptions.RegistryNotFoundException;
import com.tecweb.models.CategoryService;
import com.tecweb.repository.CategoryServiceRepository;

@Controller
@Path("/categories")
@Authenticated
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class CategoryServiceController {

  @Inject
  private CategoryServiceRepository categoryServiceRepository;

  @GET
  public MessageResponse<List<CategoryService>> findAll() {
    return MessageResponse.to(categoryServiceRepository.findAll()).build();
  }

  @GET
  @Path("/{id}")
  public MessageResponse<CategoryService> get(@PathParam("id") Long id) {
    final CategoryService categoryService = categoryServiceRepository.findOne(id);
    if (categoryService == null) {
      throw new RegistryNotFoundException();
    }
    return MessageResponse.to(categoryServiceRepository.save(categoryService)).build();
  }

  @GET
  @Path("/create")
  public MessageResponse<CategoryService> create() {
    return MessageResponse.to(new CategoryService()).build();
  }

  @POST
  @Path("/create")
  public MessageResponse<CategoryService> create(CategoryService categoryService) {
    return MessageResponse.to(categoryServiceRepository.save(categoryService))
        .message("CategoryService criado com sucesso").status(Status.CREATED).build();
  }

  @PUT
  @Path("/update")
  public MessageResponse<CategoryService> update(CategoryService categoryService) {
    return MessageResponse.to(categoryServiceRepository.save(categoryService))
        .message("Servico atualizado com sucesso").build();
  }

  @DELETE
  @Path("/{id}")
  public MessageResponse<CategoryService> delete(@PathParam("id") Long id) {
    final CategoryService categoryService = categoryServiceRepository.findOne(id);
    if (categoryService == null) {
      throw new RegistryNotFoundException();
    }
    categoryServiceRepository.delete(id);
    return MessageResponse.<CategoryService>to(null).message("CategoryService deletado com sucesso")
        .build();
  }
}
