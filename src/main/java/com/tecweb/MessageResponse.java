package com.tecweb;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlRootElement
public class MessageResponse<T> {

  public static class Builder<T> {

    private Status status;
    private List<String> messages;
    private T data;

    private Builder(T data) {
      this.status = Status.OK;
      this.messages = new LinkedList<>();
      this.data = data;
    }

    public Builder<T> status(Status status) {
      this.status = status;
      return this;
    }

    public Builder<T> message(String message) {
      this.messages.add(message);
      return this;
    }

    public Builder<T> messages(Collection<String> messages) {
      this.messages.addAll(messages);
      return this;
    }

    public MessageResponse<T> build() {
      return new MessageResponse<>(this.status, this.messages, this.data);
    }

    public Response buildResponse() {
      return Response.status(this.status).entity(build()).build();
    }
  }

  public static <T> Builder<T> to(T data) {
    return new Builder<>(data);
  }

  @XmlTransient
  private Status status;
  private List<String> messages;
  private T data;

  public MessageResponse() {}

  public MessageResponse(Status status, List<String> messages, T data) {
    this.status = status;
    this.messages = messages;
    this.data = data;
  }

  public Status getStatus() {
    return status;
  }

  public T getData() {
    return data;
  }

  public List<String> getMessages() {
    return messages;
  }
}
