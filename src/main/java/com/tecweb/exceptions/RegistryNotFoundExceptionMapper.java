package com.tecweb.exceptions;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.tecweb.MessageResponse;

@Provider
@Component
public class RegistryNotFoundExceptionMapper implements ExceptionMapper<ApplicationException> {

  private static final Logger LOG = LoggerFactory.getLogger(RegistryNotFoundExceptionMapper.class);

  @Override
  public Response toResponse(ApplicationException exception) {

    LOG.error("JAX-RS error", exception);

    final MessageResponse<Object> messageResponse =
        MessageResponse.to(null).status(Status.NOT_FOUND).build();

    return Response.status(messageResponse.getStatus()).entity(messageResponse).build();
  }
}
