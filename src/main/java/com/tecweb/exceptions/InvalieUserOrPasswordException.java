package com.tecweb.exceptions;

public class InvalieUserOrPasswordException extends ApplicationException {

  private static final long serialVersionUID = 1L;

  public InvalieUserOrPasswordException(String message) {
    super(message);
  }
}
