package com.tecweb.exceptions;

public class OutOfRatingLimitException extends ApplicationException {

  private static final long serialVersionUID = 1L;

  public OutOfRatingLimitException(String message) {
    super(message);
  }
}
