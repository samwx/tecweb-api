package com.tecweb.exceptions;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.tecweb.MessageResponse;

@Provider
@Component
public class ApplicationExceptionMapper implements ExceptionMapper<ApplicationException> {

  private static final Logger LOG = LoggerFactory.getLogger(ApplicationExceptionMapper.class);

  @Override
  public Response toResponse(ApplicationException exception) {

    LOG.error("JAX-RS error", exception);

    final MessageResponse<Object> messageResponse =
        MessageResponse.to(null).message(getStackTrace(exception)).build();

    return Response.serverError().entity(messageResponse).build();
  }

  private String getStackTrace(Throwable exception) {
    final StringWriter out = new StringWriter();
    exception.printStackTrace(new PrintWriter(out));
    return out.toString();
  }
}
