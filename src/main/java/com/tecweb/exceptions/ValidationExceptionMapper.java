package com.tecweb.exceptions;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.springframework.stereotype.Component;

import com.tecweb.MessageResponse;

@Provider
@Component
public class ValidationExceptionMapper implements ExceptionMapper<ValidationException> {

  @Override
  public Response toResponse(ValidationException exception) {
    return MessageResponse.to(null).status(Status.BAD_REQUEST).messages(toMessages(exception))
        .buildResponse();
  }

  private List<String> toMessages(ValidationException exception) {
    if (exception instanceof ConstraintViolationException) {
      return toMessages(((ConstraintViolationException) exception).getConstraintViolations());
    }
    return Collections.singletonList(exception.toString());
  }

  private List<String> toMessages(Set<ConstraintViolation<?>> constraintViolations) {
    return constraintViolations.stream().map(Object::toString).collect(Collectors.toList());
  }
}
